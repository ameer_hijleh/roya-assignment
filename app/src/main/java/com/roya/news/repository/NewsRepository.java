package com.roya.news.repository;

import com.roya.news.MyApplication;
import com.roya.news.db.AppDatabase;
import com.roya.news.model.BaseNews;
import com.roya.news.model.News;
import com.roya.news.network.ApiInterface;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * NewsRepository is a class that we need to be a new layer
 * gives us additional level of abstraction over data access .
 * @see NewsRepository
 */
public class NewsRepository {
    private static NewsRepository instance;
    private ApiInterface apiInterface;
    private AppDatabase database;


    /**
     * NewsRepository() is a constructor we will initialise the ApiInterface and AppDatabase.
     */
    public NewsRepository() {
        this.apiInterface = MyApplication.getInstance().getApi();
        this.database = MyApplication.getInstance().getDatabase();
    }

    public static synchronized NewsRepository getInstance() {
        if (instance == null) {
            instance = new NewsRepository();
        }
        return instance;
    }

    /**
     * getNewsApi will call news API by apiInterface.getNews(pageId)
     * @param pageID The page that will need to load the next page.
     * @return MutableLiveData<List<News>>
     */
    public MutableLiveData<List<News>> getNewsApi(int pageID) {
        MutableLiveData<List<News>> data = new MutableLiveData<>();
        apiInterface.getNews(pageID).enqueue(new Callback<BaseNews>() {
            //
            @Override
            public void onResponse(@NonNull Call<BaseNews> call, @NonNull Response<BaseNews> response) {
                if (response.body() != null) {
                    data.setValue(response.body().newsList);
                    insertNews(response.body().newsList);
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseNews> call, @NonNull Throwable t) {
            }
        });
        return data;
    }

    /**
     * getNewsDB will fetch news for the page we need
     * @param pageID The page that will need to load the next page.
     * @return MutableLiveData<List<News>>
     */
    public MutableLiveData<List<News>> getNewsDB(int pageID) {
        MutableLiveData<List<News>> data = new MutableLiveData<>();
        new Thread(() -> {
            int end = 76 * pageID;
            int start = 1;
            if (pageID != 1) {
                start = end - 76;
            }
            data.postValue(database.newsDao().getAll(start, end));
        }).start();

        return data;
    }

    /**
     * insertNews will insert news list in room database
     * @param list The list that will need to insert it to database.
     */
    private void insertNews(List<News> list) {
        new Thread(() -> database.newsDao().insertAll(list)).start();

    }
}
