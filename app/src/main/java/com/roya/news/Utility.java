package com.roya.news;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import androidx.databinding.BindingAdapter;

public class Utility {

    /**
     * Will call when we set "imageUrl" in imageView in XML .
     * @param view The view we need to add new attribute "imageUrl".
     * @param url The url we pass it from xml.
     */
    @BindingAdapter({"imageUrl"})
    public static void loadImage(ImageView view, String url) {
        Picasso.get().load(url).placeholder(R.drawable.ic_loading).error(R.drawable.ic_newspaper).into(view);

    }

    /**
     * check if internet connection is connected.
     */
    @SuppressLint("NewApi")
    public static boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) MyApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nInfo = cm.getActiveNetworkInfo();
        return nInfo != null && nInfo.isAvailable() && nInfo.isConnected();
    }
}
