package com.roya.news.ui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.roya.news.R;
import com.roya.news.databinding.NewsItemBinding;
import com.roya.news.model.News;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NewsAdapter extends RecyclerView.Adapter<NewsViewHolder> {

    private List<News> list;

    public NewsAdapter(List<News> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return new NewsViewHolder(NewsItemBinding.inflate(layoutInflater, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        holder.bind(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addItems(List<News> postItems) {
        if (postItems != null) {
            list.addAll(postItems);
            notifyDataSetChanged();
        }
    }

}