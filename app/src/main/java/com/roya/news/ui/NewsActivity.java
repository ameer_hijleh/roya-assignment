package com.roya.news.ui;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.roya.news.R;
import com.roya.news.databinding.ActivityMainBinding;
import com.roya.news.ui.adapter.NewsAdapter;
import com.roya.news.ui.adapter.PaginationListener;

import java.util.ArrayList;

import static com.roya.news.ui.adapter.PaginationListener.PAGE_START;

public class NewsActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    NewsViewModel newsViewModel;
    NewsAdapter adapter;
    private int currentPage = PAGE_START;
    private boolean isLastPage = false;
    private boolean isLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        init();
        setNewsList();
    }

    /**
     * Will initialise the instance variable we need.
     */
    private void init() {
        newsViewModel = new NewsViewModel();
        adapter = new NewsAdapter(new ArrayList<>());
    }

    /**
     * will set recyclerView adapter news and add a listener in recyclerView for scroll view
     * and call onLoadData().
     * addOnScrollListener we need it to listen when we scroll down for pagination,
     * this listener that will be notified of any changes in scroll state or position
     */
    private void setNewsList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recyclerView.setLayoutManager(layoutManager);
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.addOnScrollListener(new PaginationListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                ++currentPage;
                binding.progrees.setVisibility(View.VISIBLE);

                onLoadData();
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
        onLoadData();

    }

    /**
     * Will run when we need to load new news.
     */
    public void onLoadData() {
        newsViewModel.getNews(currentPage).observe(this, news -> {
            isLastPage = news.isEmpty();
            binding.progrees.setVisibility(View.GONE);
            adapter.addItems(news);
            adapter.notifyDataSetChanged();

        });

    }
}
