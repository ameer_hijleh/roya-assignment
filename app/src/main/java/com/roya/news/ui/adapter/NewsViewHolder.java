package com.roya.news.ui.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.roya.news.databinding.NewsItemBinding;
import com.roya.news.model.News;

public class NewsViewHolder extends RecyclerView.ViewHolder {

    private NewsItemBinding binding;

    NewsViewHolder(@NonNull NewsItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(News movie) {
        binding.setNews(movie);
        binding.executePendingBindings();
    }
}
